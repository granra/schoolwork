08048c11 <phase_6>:
 	push   %ebp
 	mov    %esp,%ebp
 	push   %edi
 	push   %esi
 	push   %ebx
 	sub    $0x4c,%esp
 	lea    -0x30(%ebp),%eax
 	mov    %eax,0x4(%esp)
 	mov    0x8(%ebp),%eax
 	mov    %eax,(%esp)
 	call   804911a <read_six_numbers>
 	mov    $0x0,%edi
 	lea    -0x30(%ebp),%esi
 	mov    (%esi,%edi,4),%eax
 	sub    $0x1,%eax
 	cmp    $0x5,%eax
 	jbe    8048c44 <phase_6+0x33>
 	call   80490d8 <explode_bomb>
 	add    $0x1,%edi
 	cmp    $0x6,%edi
 	je     8048c66 <phase_6+0x55>
 	mov    %edi,%ebx
 	mov    -0x4(%esi,%edi,4),%eax
 	cmp    (%esi,%ebx,4),%eax
 	jne    8048c5c <phase_6+0x4b>
 	call   80490d8 <explode_bomb>
 	add    $0x1,%ebx
 	cmp    $0x5,%ebx
 	jle    8048c4e <phase_6+0x3d>
 	jmp    8048c34 <phase_6+0x23>
 	lea    -0x30(%ebp),%eax
 	lea    -0x18(%ebp),%ebx
 	mov    $0x7,%ecx
 	mov    %ecx,%edx
 	sub    (%eax),%edx
 	mov    %edx,(%eax)
 	add    $0x4,%eax
 	cmp    %ebx,%eax
 	jne    8048c71 <phase_6+0x60>
 	mov    $0x0,%ebx
 	lea    -0x30(%ebp),%edi
 	jmp    8048c9e <phase_6+0x8d>
 	mov    0x8(%edx),%edx
 	add    $0x1,%eax
 	cmp    %ecx,%eax
 	jne    8048c88 <phase_6+0x77>
 	mov    %edx,-0x48(%ebp,%esi,4)
 	add    $0x1,%ebx
 	cmp    $0x6,%ebx
 	je     8048cb4 <phase_6+0xa3>
 	mov    %ebx,%esi
 	mov    (%edi,%ebx,4),%ecx
 	mov    $0x1,%eax
 	mov    $0x804b5dc,%edx
 	cmp    $0x1,%ecx
 	jg     8048c88 <phase_6+0x77>
 	jmp    8048c92 <phase_6+0x81>
 	mov    -0x48(%ebp),%ebx
 	mov    -0x44(%ebp),%eax
 	mov    %eax,0x8(%ebx)
 	mov    -0x40(%ebp),%edx
 	mov    %edx,0x8(%eax)
 	mov    -0x3c(%ebp),%eax
 	mov    %eax,0x8(%edx)
 	mov    -0x38(%ebp),%edx
 	mov    %edx,0x8(%eax)
 	mov    -0x34(%ebp),%eax
 	mov    %eax,0x8(%edx)
 	movl   $0x0,0x8(%eax)
 	mov    $0x0,%esi
 	mov    0x8(%ebx),%eax
 	mov    (%ebx),%edx
 	cmp    (%eax),%edx
 	jge    8048cef <phase_6+0xde>
 	call   80490d8 <explode_bomb>
 	mov    0x8(%ebx),%ebx
 	add    $0x1,%esi
 	cmp    $0x5,%esi
 	jne    8048ce1 <phase_6+0xd0>
 	add    $0x4c,%esp
 	pop    %ebx
 	pop    %esi
 	pop    %edi
 	pop    %ebp
 	ret    