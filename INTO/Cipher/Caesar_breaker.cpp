/**
 * Program to encrypt and decrypt strings using the Caesar Cipher.
 * Input: mode(encrypt/decrypt), shift value, string.
 * Output: encrypted/decrypted string.
 * Created by: Arnar Gauti Ingason og Anton Marínó Stefánsson.
 */

#include <iostream>
#include <string.h>
using namespace std;

// Takes in an alphabet and a shift value and creates a new alphabet used for encrypting/decrypting according
// to the keyword subtitution cipher.
void newAlpha(char alphabet[], char newAlphabet[], int shift);

// Takes in a char array and changes every char to either upper case or lower case depending on preference.
// Second parameter is used to select mode, 'U' to change to upper case and 'L' to change to lower case.
void letterCase(char input[], char mode);

// Takes in a regular alphabet and an encryption alphabet. Third parameter is an integer to select between
// encryption and decryption. 1 encrypts and 2 decrypts. The function returns the encrypted/decrypted string.
string crypt(char alphabet[], char newAlphabet[], int mode, char input[]);

int main()
{
	char newAlphabet[27], alphabet[27] = "abcdefghijklmnopqrstuvwxyz";

	char origInput[10000];
	cin.read(origInput, 10000);

	for (int i = 1; i < 26; ++i)
	{
		newAlpha(alphabet, newAlphabet, i);
		letterCase(newAlphabet, 'U');

		//cout << alphabet << endl << newAlphabet << endl;
		cout << crypt(alphabet, newAlphabet, 2, origInput) << endl;
	}
	
	return 0;
}

void newAlpha(char alphabet[], char newAlphabet[], int shift)
{
	int counter(0);
	
	for (int i = shift; i < 26; ++i)
	{
		newAlphabet[counter] = alphabet[i];
		counter++;
		
		if (i == 25)
		{
			for (int k = 0; k < shift; ++k)
			{
				newAlphabet[counter] = alphabet[k];
				counter++;
			}
		}
	}
}

void letterCase(char input[], char mode)
{
	int length = strlen(input);
	
	if(mode == 'U')
	{
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 96 && input[i] < 123){
				input[i] = char(input[i] - 32);
			}
		}
	}
	else if(mode == 'L')
	{
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 64 && input[i] < 91){
				input[i] = char(input[i] + 32);
			}
		}
	}
}

string crypt(char alphabet[], char newAlphabet[], int mode, char input[]){
	//cout << input << endl;
	int length = strlen(input);
	char output[10000];

	if(mode == 1) // encrypt
	{
		letterCase(input, 'L');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 26; ++j)
			{
				if(input[i] == alphabet[j]){
					output[i] = newAlphabet[j];
				}
			}
		}
	}
	else if(mode == 2) // decrypt
	{
		letterCase(input, 'U');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 26; ++j)
			{
				if(input[i] == newAlphabet[j])
				{
					output[i] = alphabet[j];
				}
			}
		}
	}

	return output;
}