#include <iostream>
#include <string.h>
using namespace std;

void newAlpha(char alphabet[], char newAlphabet[], char keyphrase[]);

void letterCase(char input[], char mode);

string crypt(char alphabet[], char newAlphabet[], int mode);

void removeDuplicates(char keyphrase[]);

int main()
{
	char keyphrase[100], newAlphabet[27], alphabet[27] = "abcdefghijklmnopqrstuvwxyz";
	int mode;

	cin >> mode;
	cin >> keyphrase;

	letterCase(keyphrase, 'L');
	removeDuplicates(keyphrase);
	newAlpha(alphabet, newAlphabet, keyphrase);
	letterCase(newAlphabet, 'U');
	cout << alphabet << endl << newAlphabet << endl;
	//crypt(alphabet, newAlphabet, mode);


	cout << crypt(alphabet, newAlphabet, mode) << endl;
	return 0;
}

void newAlpha(char alphabet[], char newAlphabet[], char keyphrase[]){
	int index, counter(0), keyLength = strlen(keyphrase);
	for (index = 0; index < keyLength; ++index)
	{
		newAlphabet[index] = keyphrase[index];
	}
	newAlphabet[index] = 0;

	while(newAlphabet[index - 1] != alphabet[counter]){
		counter++;
	}

	if (counter == 25)
	{
		counter = 0;
	}

	for (int i = counter; i < 26; ++i)
	{
		bool duplicate(1);
		int count(0);
		while(newAlphabet[count] != 0){
			if (alphabet[i] == newAlphabet[count])
			{
				duplicate = 0;
			}
			count++;
		}
		if (duplicate)
		{
			newAlphabet[index] = alphabet[i];
			index++;
			newAlphabet[index] = 0;
		}
	}

	for (int i = 0; i < counter; ++i)
	{
		bool duplicate(1);
		int count(0);
		while(newAlphabet[count] != 0){
			if (alphabet[i] == newAlphabet[count])
			{
				duplicate = 0;
			}
			count++;
		}
		if (duplicate)
		{
			newAlphabet[index] = alphabet[i];
			index++;
			newAlphabet[index] = 0;
		}
	}
}

void letterCase(char input[], char mode){
	int length = strlen(input);
	if(mode == 'U'){
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 96 && input[i] < 123){
				input[i] = char(input[i] - 32);
			}
		}
	}
	else if(mode == 'L'){
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 64 && input[i] < 91){
				input[i] = char(input[i] + 32);
			}
		}
	}
}

string crypt(char alphabet[], char newAlphabet[], int mode){
	char input[10000];
	cin.ignore();
	cin.read(input, 10000);

	int length = strlen(input);

	if(mode == 1){ // encrypt
		letterCase(input, 'L');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 26; ++j)
			{
				if(input[i] == alphabet[j]){
					input[i] = newAlphabet[j];
				}
			}
		}
	}
	else if(mode == 2){ // decrypt
		letterCase(input, 'U');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 26; ++j)
			{
				if(input[i] == newAlphabet[j]){
					input[i] = alphabet[j];
				}
			}
		}
	}

	return input;
}

void removeDuplicates(char keyphrase[]){
	char temp[100];
	temp[0] = 0;
	int tmpIndex(0), length = strlen(keyphrase);
	for (int i = 0; i < length; ++i)
	{
		bool duplicate(1);
		int counter(0);
		while(temp[counter] != 0){
			if(temp[counter] == keyphrase[i]){
				duplicate = 0;
			}
			counter++;
		}
		if (duplicate)
		{
			temp[tmpIndex] = keyphrase[i];
			tmpIndex++;
			temp[tmpIndex] = 0;
		}
	}

	strcpy(keyphrase, temp);
}