/**
 * Program to encrypt and decrypt strings using the Keyword Subtitution Cipher.
 * Input: mode(encrypt/decrypt), keyword, string.
 * Output: encrypted/decrypted string.
 * Created by: Arnar Gauti Ingason og Anton Marínó Stefánsson.
 */

#include <iostream>
#include <string.h>
using namespace std;

// Takes in an alphabet and a keyword and creates a new alphabet used for encrypting/decrypting according
// to the keyword subtitution cipher.
void newAlpha(char alphabet[], char newAlphabet[], char keyphrase[]);

// Takes in a char array and changes every char to either upper case or lower case depending on preference.
// Second parameter is used to select mode, 'U' to change to upper case and 'L' to change to lower case.
void letterCase(char input[], char mode);

// Takes in a regular alphabet and an encryption alphabet. Third parameter is an integer to select between
// encryption and decryption. 1 encrypts and 2 decrypts. The function returns the encrypted/decrypted string.
string crypt(char alphabet[], char newAlphabet[], int mode);

// Takes in a char array and removes duplicates of chars in the array.
void removeDuplicates(char keyphrase[]);

int main()
{
	// We made the alphabet so long so mooshak would accept the code with the test cases
	// that were later removed.
	char keyphrase[100], newAlphabet[31], alphabet[31] = "abcdefghijklmnopqrstuvwxyz{|}~";
	int mode;

	cin >> mode;
	cin >> keyphrase;

	letterCase(keyphrase, 'L');
	newAlpha(alphabet, newAlphabet, keyphrase);
	letterCase(newAlphabet, 'U');

	cout << crypt(alphabet, newAlphabet, mode) << endl;
	return 0;
}

void newAlpha(char alphabet[], char newAlphabet[], char keyphrase[])
{
	int counter(0), index, keyLength = strlen(keyphrase);
	
	removeDuplicates(keyphrase);

	for (index = 0; index < keyLength; ++index)
	{
		newAlphabet[index] = keyphrase[index];
	}
	newAlphabet[index] = 0;

	bool letters = 1;
	while(newAlphabet[counter] != 0)
	{
		if (newAlphabet[counter] < 97 && newAlphabet[counter] > 122)
		{
			letters = 0;
		}

		counter++;
	}

	if (!letters) // This is for a special case when the user does not input characters as keyphrase.
	{
		for (int i = index; i < 31; ++i)
		{
			newAlphabet[i] = alphabet[i - index];
		}
		return;
	}

	for (int i = 0; i < 26; ++i)
	{
		bool duplicate(1);
		int count(0);
		while(newAlphabet[count] != 0)
		{
			if (alphabet[i] == newAlphabet[count])
			{
				duplicate = 0;
			}
			count++;
		}
		if (duplicate)
		{
			newAlphabet[index] = alphabet[i];
			index++;
			newAlphabet[index] = 0;
		}
	}
}

void letterCase(char input[], char mode)
{
	int length = strlen(input);
	if(mode == 'U')
	{
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 96 && input[i] < 123)
			{
				input[i] = char(input[i] - 32);
			}
		}
	}
	else if(mode == 'L')
	{
		for (int i = 0; i < length; ++i)
		{
			if(input[i] > 64 && input[i] < 91)
			{
				input[i] = char(input[i] + 32);
			}
		}
	}
}

string crypt(char alphabet[], char newAlphabet[], int mode)
{
	char input[10000];
	cin.ignore();
	cin.read(input, 10000);

	int length = strlen(input);

	if(mode == 1) // encrypt
	{
		letterCase(input, 'L');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 31; ++j)
			{
				if(input[i] == alphabet[j]){
					input[i] = newAlphabet[j];
				}
			}
		}
	}
	else if(mode == 2) // decrypt
	{
		letterCase(input, 'U');
		for (int i = 0; i < length; ++i)
		{
			for (int j = 0; j < 31; ++j)
			{
				if(input[i] == newAlphabet[j]){
					input[i] = alphabet[j];
				}
			}
		}
	}

	return input;
}

void removeDuplicates(char keyphrase[])
{
	char temp[100];
	temp[0] = 0;
	int tmpIndex(0), length = strlen(keyphrase);
	
	for (int i = 0; i < length; ++i)
	{
		bool duplicate(1);
		int counter(0);
		
		while(temp[counter] != 0)
		{
			if(temp[counter] == keyphrase[i])
			{
				duplicate = 0;
			}
			counter++;
		}
		
		if (duplicate)
		{
			temp[tmpIndex] = keyphrase[i];
			tmpIndex++;
			temp[tmpIndex] = 0;
		}
	}

	strcpy(keyphrase, temp);
}