// Notes and their frequency.
#define C 131
#define D 147
#define E 165
#define F 175
#define G 196
#define A 220
#define B 247
// Pin connected to ST_CP of 74HC595.
#define latchPin 8 // Brown
// Pin connected to SH_CP of 74HC595.
#define clockPin 12 // Orange
// Pin connected to DS of 74HC595.
#define dataPin 11 // Yellow

// Pins for RGB led.
#define bluePin 7
#define greenPin 6
#define redPin 5
// Pin for the buzzer.
#define buzzPin 3
// Pin for the button.
#define buttonPin 2

                  // Hex values we push to the shift register to display each number.
                  // 0    1     2     3     4     5     6     7     8     9
int numbers[10] = {0xEE, 0x22, 0xD6, 0x76, 0x3A, 0x7C, 0xFC, 0x26, 0xFE, 0x7E};

                      // Our RGB led is common-cathode so writing the pins high
                      // turns on individual colors.
                      // blue    green     red
int ledStates[10][3] =  {{0,       0,       0}, // 0
                         {0,       0,       1}, // 1
                         {0,       1,       0}, // 2
                         {0,       1,       1}, // 3
                         {1,       0,       0}, // 4
                         {1,       0,       1}, // 5
                         {1,       1,       0}, // 6
                         {1,       1,       1}, // 7
                         {0,       0,       0}, // 8
                         {0,       0,       0}};// 9

int activeNumber = 10;
int inByte = 0;
int currentOctave = 1;
bool clicked = 0;

// Sets the display to a number you input.
void updateDisplay(int number);

// Sets the RGB led to a specific color with each number
// according to assignment description.
void updateRGB(int number);

// Increments the number displayed on the 7-segment display
// and RGB led.
void incrementDisplay();

// This function reads the button state and increments the
// display if the button is pressed.
void readButton();

// This function listens to the serial bus for certain messages.
void readSerial();

void setup() {
  // Set pins for Shift Register as outputs.
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  // Set pins for RGB led as outputs.
  pinMode(bluePin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(redPin, OUTPUT);
  // Prepare button.
  pinMode(buttonPin, INPUT);
  // Set display to 0.
  incrementDisplay();
  // Open serial port for bluetooth communication.
  Serial.begin(9600);
}

void loop() {
  readButton();
  readSerial();
}



void updateDisplay(int number) {
  digitalWrite(latchPin, LOW);
      
  shiftOut(dataPin, clockPin, LSBFIRST, numbers[number]);
      
  digitalWrite(latchPin, HIGH);
}

void updateRGB(int number) {
  digitalWrite(bluePin, ledStates[number][0]);
  digitalWrite(greenPin, ledStates[number][1]);
  digitalWrite(redPin, ledStates[number][2]); 
}

void incrementDisplay() {
  if(activeNumber >= 0 && activeNumber < 9){
    activeNumber++;
  }
  else {              // If activeNumber is out of 0-9 range it becomes 0.
    activeNumber = 0; // e.g. when it's 9 it should become 0 again.
  }
  
  updateDisplay(activeNumber);
  updateRGB(activeNumber);
}

void readButton() {
  if (!clicked) {                 // If button was not previously clicked
    if (digitalRead(buttonPin)) { // but is now.
      incrementDisplay();
      clicked = 1;
    }
  }
  else if (clicked) {             // If button was previously clicked
    if(!digitalRead(buttonPin)) { // but is not anymore.
        clicked = 0;
        delay(100); // Debounce.
    }  
  }
}

void readSerial() {
  if(Serial.available() > 0) {
    inByte = Serial.read() - 48;
    if(inByte >= 0 && inByte < 10) {
      updateDisplay(inByte);
      updateRGB(inByte);
      activeNumber = inByte;
    }

    switch (inByte + 48) {
        case 97: // a in ascii
          tone(buzzPin, C << currentOctave, 500); // Note C
          break;
        case 115: // s in ascii
          tone(buzzPin, D << currentOctave, 500); // Note D
          break;
        case 100: // d in ascii
          tone(buzzPin, E << currentOctave, 500); // Note E
          break;
        case 102: // f in ascii
          tone(buzzPin, F << currentOctave, 500); // Note F
          break;
        case 103: // g in ascii
          tone(buzzPin, G << currentOctave, 500); // Note G
          break;
        case 104: // h in ascii
          tone(buzzPin, A << currentOctave, 500); // Note A
          break;
        case 106: // j in ascii
          tone(buzzPin, B << currentOctave, 500); // Note B
          break;
        case 110: // n in ascii
          if(currentOctave < 3) {
            currentOctave++;
          }
          break;
        case 109: // m in ascii
          if(currentOctave > 0) {
            currentOctave--;
          }
          break;
        default:
          break;
    }
  }
}