#include <iostream>
using namespace std;

int main()
{
	int n;
	double r, guess(2), prevGuess;
	cin >> n;
	do{
		prevGuess = guess;
		r = n/guess;
		guess = (guess + r) / 2;
	}while(prevGuess*1.01 < guess || prevGuess*0.99 > guess);
	cout << guess;

	return 0;
}