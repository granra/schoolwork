#include <iostream>
using namespace std;

int main()
{
	double r, n, guess(2), prevGuess;
	cin >> n;
	for (int i = 0; i < 100; ++i)
	{
		r = n/guess;
		guess = (guess + r) / 2;
		if(prevGuess*1.01 > guess && prevGuess*0.99 < guess){
			break;
		}
		prevGuess = guess;
	}
	cout << guess;

	return 0;
}