#include <iostream>
using namespace std;

int main(){
    int n;
    cin >> n;
    cout << endl << "The answer to life, the universe, and everything is " << n << ".";

    return 0;
}
