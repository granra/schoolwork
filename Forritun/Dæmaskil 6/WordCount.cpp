#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

void file(ifstream& inn, ofstream& out, char input[], char output[], int openclose);

// This function counts and returns the number of lines in a given text file.
int lines(ifstream& inn);

// This function counts and returns the number of words in a given text file.
int words(ifstream& inn);

// This function counts and returns the number of characters in a given text file.
int characters(ifstream& inn);

void print(int lines, int words, int characters, ofstream& out);

int main(){
	ifstream fin;
	ofstream fout;
	char input[20], output[20];
	int lineCount, wordCount, characterCount;

	cout << "What's the filename? ";
	cin >> input;
	cout << "Where should I output the results? ";
	cin >> output;
	file(fin, fout, input, output, 1); // Opening the text files.
	lineCount = lines(fin);
	wordCount = words(fin);
	characterCount = characters(fin);
	print(lineCount, wordCount, characterCount, fout);
	file(fin, fout, input, output, 0); // Closing the text files.
	return 0;
}

void file(ifstream& inn, ofstream& out, char input[], char output[], int openclose){
	if(openclose){
		inn.open(input); // Opening the text file to read from.
		if(inn.fail()){ // fin.fail() returns true if there is a problem opening the file.
			cout << "Can't find and/or open the file." << endl;
			exit(1);
		}
		out.open(output); // Opening the text file to write to.
		if(out.fail()){
			cout << "Can't find and/or open the file." << endl;
			exit(1);
		}
	}
	else{
		out.close();
		inn.close();
	}
}

int lines(ifstream& inn){
	int temp(0); // Variable for storing the number of lines.
	char takn;
	inn.get(takn);
	if(!inn.eof()){
		char line[256];
		while(!inn.eof()){ // inn.eof() returns true if the end of file is reached.
			temp++;
			inn.getline(line, 256);
		}
		inn.seekg(0, inn.beg); // This goes back to the beginning of the text file.
	}
	return temp;
}

int words(ifstream& inn){
	int temp(0);
	char takn;
	inn.get(takn);
	if(!inn.eof()){
		string takn;
  		while (!inn.eof()){
    		temp++;
    		inn >> takn;
  		}
		inn.seekg(0, inn.beg);
  	}
	return temp;
}

int characters(ifstream& inn){
	int temp(0);
	char takn;
	inn.get(takn);
  	while (!inn.eof()){
    	temp++;
    	inn.get(takn);
  	}
	inn.seekg(0, inn.beg);
	return temp;
}

void print(int lines, int words, int characters, ofstream& out){
	cout << "The input contains:" << endl;
	cout << lines << " lines" << endl;
  	cout << words << " words" << endl;
  	cout << characters << " characters" << endl;

	out << "The input contains:" << endl;
	out << lines << " lines" << endl;
  	out << words << " words" << endl;
  	out << characters << " characters" << endl;
}