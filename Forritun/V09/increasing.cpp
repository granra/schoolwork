#include <iostream>
using namespace std;

// TODO: function declaratin comes here
bool increasing(int array[100], int n);

int main()
{
    int arr[100], n;
    while (cin >> n)
    {
        for (int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        cout << (increasing(arr, n) ? "increasing" : "not increasing") << endl;
    }

    return 0;
}

// TODO: function implementation comes here
bool increasing(int array[100], int n){
	int truth = 1;
	for (int i = 1; i < n; ++i)
	{
		if((array[i - 1] > array[i]) & (array[i -1] != array[i])){
			truth = 0;
		}
	}
	return truth;
}