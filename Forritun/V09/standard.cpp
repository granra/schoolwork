#include <iostream>
#include <cmath>
using namespace std;

int main()
{
	int n;
	double numbers[100], sum(0), average, temp;
	cin >> n;

	for (int i = 0; i < n; ++i)
	{
		cin >> numbers[i];
	}

	for (int i = 0; i < n; ++i)
	{
		sum += numbers[i];
	}
	average = sum / n;

	sum = 0;
	for (int i = 0; i < n; ++i)
	{
		temp = numbers[i] - average;
		temp *= temp;
		sum += temp;
	}

	cout << sqrt(sum / n);
	
	return 0;
}
