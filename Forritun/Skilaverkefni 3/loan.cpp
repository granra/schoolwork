#include <iostream>
#include <iomanip>
using namespace std;

class Loan {
private:
	double prinAmount;
	double annInterest;
	double numInstall;
	double computeInstalment();
	double computeBalance(int numPayment);
	double computeInterest(int numPayment);
public:
	Loan();
	Loan(double principal, double interest, double numPay);
	void displaySchedule();
};

int main()
{
	double principal, interest, numPay;
	cout << "Please input data for the loan" << endl;
	cout << "Principal: ";
	cin >> principal;
	cout << "Interest rate: ";
	cin >> interest;
	cout << "Number of payments: ";
	cin >> numPay;

	Loan loan(principal, interest, numPay);
	loan.displaySchedule();

	return 0;
}

Loan::Loan() {
	prinAmount = 0;
	annInterest = 0;
	numInstall = 0;
}

Loan::Loan(double principal, double interest, double numPay) {
	prinAmount = principal;
	annInterest = interest;
	numInstall = numPay;
}

double Loan::computeInstalment() {
	return prinAmount / numInstall;
}

double Loan::computeInterest(int numPayment) {
	return computeBalance(numPayment) * (annInterest / 12) / 100;
}

double Loan::computeBalance(int numPayment) {
	return prinAmount - (numPayment - 1) * computeInstalment();
}

void Loan::displaySchedule() {
	double instal = computeInstalment();
	double overallInst = 0, overallInter = 0;

	cout << setw(6) << "No."
		 << setw(14) << "Principal"
		 << setw(14) << "Instalment"
		 << setw(14) << "Interest"
		 << setw(6) << ""  // Space between Interest and Total payment since the last column is left justified
		 << "Total payment" << endl;
	cout << setfill('-') << setw(68) << "" << endl;  // Draw a line
	cout << fixed << setprecision(2) << setfill(' ');

	for (int i = 1; i <= numInstall; ++i)
	{
		double prin = computeBalance(i);
		double inter = computeInterest(i);

		cout << setw(6) << i
			 << setw(14) << prin
			 << setw(14) << instal
			 << setw(14) << inter
			 << setw(6) << ""
			 << instal + inter << endl;

		overallInter += inter;
		overallInst += instal;
	}

	cout << setfill('-') << setw(68) << "" << endl; // Draw a line
	cout << setfill(' ')
		 << setw(34) << overallInst
		 << setw(14) << overallInter
		 << setw(6) << ""
		 << overallInst + overallInter << endl;
}