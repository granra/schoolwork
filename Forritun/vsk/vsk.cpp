#include <iostream>
#include <iomanip>
using namespace std;

int main() {
	int tala;
	cin >> tala;
	cout << fixed << setprecision(2) << tala * 1.255;

	return 0;
}
