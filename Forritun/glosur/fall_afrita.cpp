#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;
void afrita (ifstream& inn, ofstream& ut);
int main()
{

  ifstream fin;
  cout << "Hvad heitir inntaksskrain? ";
  char inntak[20];
  cin >> inntak;
  fin.open(inntak);
  if (fin.fail())
  {
      cout << "skrain " << inntak << " opnast ekki" << endl;
      exit(1);
  }
  ofstream fout;
  cout << "Hvad heitir uttaksskrain? ";
  char uttak[20];
  cin >> uttak;
  fout.open(uttak);
  afrita(fin,fout);
  fin.close();
  fout.close();
  return 0;
}
void afrita (ifstream& inn, ofstream& ut)
{
  char takn;
  inn >> takn;
  while (!inn.eof()){
    cout << takn;
    ut << takn;
    inn.get(takn);
  }
  return;
}
