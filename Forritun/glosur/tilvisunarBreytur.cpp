#include <iostream>
using namespace std;
void umOgFlatFern(double len, double bre, double& ummal, double& flatarmal);
int main()
{
  cout << "Byrjun forrits" << endl;
  double lengd;
  cout << "Slaid inn lengd: ";
  cin >> lengd;
  double breidd;
  cout << "Slaid inn breidd: ";
  cin >> breidd;
  double ummal;
  double flatarmal;
  umOgFlatFern(lengd,breidd, ummal,flatarmal);
  cout << "Ummal ferhyrningsins er " << ummal << endl;
  cout << "Flatarmal ferhyrningsins er " << flatarmal << endl;

  cout << "Forrit buid" << endl;
  return 0;
}
void umOgFlatFern(double len, double bre, double& ummal, double& flatarmal)
{
    flatarmal = len*bre;
    ummal = 2*len + 2*bre;
    return;
}
