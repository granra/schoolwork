#include <iostream>
using namespace std;

void lesa_einkunn(int fylki[], int size, int min, int max);

void skrifa_einkunn(int fylki[], int size);

double medaltal(int fylki[], int size);

int main(){
	const int NEMAR = 3;
	int einkunn[NEMAR];
	lesa_einkunn(einkunn,NEMAR, 0, 10);
	skrifa_einkunn(einkunn,NEMAR);
	double medaleinkunn = medaltal(einkunn,NEMAR);
	cout << "Meðaleinkunn er " << medaleinkunn << endl;
	return 0;
}

void lesa_einkunn(int fylki[], int size, int min, int max){
	for(int i=0; i<size; i++){
		cout << "Settu inn einkunn nemanda nr. " << i+1 << " ";
		do{
			cin >> fylki[i];
			if(fylki[i] < min || fylki[i] > max){
				cout << " Skráið inn einkunn á bilinu " << min << " til " << max << endl;
			}
		}while(fylki[i] < min || fylki[i] > max);
	}
}

void skrifa_einkunn(int fylki[], int size){
	cout << "Einkunnir nemanda eru ";
	for(int i=0; i<size; i++){
		cout <<  fylki[i] << " ";
	}
	cout << endl;
}

double medaltal(int fylki[], int size){
	double summa(0);
	for(int i=0; i<size; i++){
		summa += fylki[i];
	}
	return summa / size;
}