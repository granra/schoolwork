#include <iostream>
using namespace std;

// TODO: implement the too_heavy function here
int too_heavy(int weight, int height){
    height -= 100;
    if (height <= weight)
    {
        return true;
    }
    else{
        return false;
    }
}
// DO NOT EDIT BELOW THIS LINE
// ---------- snip -----------

int main()
{
    int weight, height;
    while (cin >> weight >> height)
    {
        cout << "too_heavy(" << weight << ", " << height << ") = ";

        if (too_heavy(weight, height))
        {
            cout << "true" << endl;
        }
        else
        {
            cout << "false" << endl;
        }
    }

    return 0;
}