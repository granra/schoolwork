#include <iostream>
using namespace std;

// TODO: implement the is_divisible_by function here
int is_divisible_by(int a, int b){
    if (a%b == 0)
    {
        return true;
    }
    else{
        return false;
    }
}
// TODO: implement the range_sum function here
int range_sum(int n, int m, int x, int y){
    int sum(0);
    for (int i = n; i <= m; ++i)
    {
        if (is_divisible_by(i,x) || is_divisible_by(i,y))
        {
            sum += i;
        }
    }
    return sum;
}
// DO NOT EDIT BELOW THIS LINE
// ---------- snip -----------

int main()
{
    int cnt;
    cin >> cnt;
    while (cnt--)
    {
        int p, q;
        cin >> p >> q;
        cout << "is_divisible_by(" << p << ", " << q << ") = ";
        if (is_divisible_by(p, q))
        {
            cout << "true" << endl;
        }
        else
        {
            cout << "false" << endl;
        }
    }

    cin >> cnt;
    while (cnt--)
    {
        int n, m, x, y;
        cin >> n >> m >> x >> y;
        cout << "range_sum(" << n << ", " << m << ", " << x << ", " << y << ") = ";
        cout << range_sum(n, m, x, y) << endl;
    }

    return 0;
}