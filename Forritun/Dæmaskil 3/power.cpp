#include <iostream>
using namespace std;

void input(int& base, int& exponent);

double calculation(int base, int exponent);

void output(int base, int exponent, double power);

int main()
{
	int base, exponent;
	double power;
	input(base, exponent);
	power = calculation(base, exponent);
	output(base, exponent, power);
	return 0;
}

void input(int& base, int& exponent){
	cout << "What's the base? ";
	cin >> base;
	cout << "What's the exponent? ";
	cin >> exponent;
}

double calculation(int base, int exponent){
	if(exponent == 0){
		return 1;
	}
	else{
		double temp = base;
		if(exponent > 0){
			for(int i=1; i<exponent; i++){
				temp *= base;
			}
		}
		else{
			for(int i=0; i>=exponent; i--){
				temp /= base;
			}
		}
		return temp;
	}
}

void output(int base, int exponent, double power){
	cout << base << " to the power of " << exponent << " is " << power << endl;
}