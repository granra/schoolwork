#include <iostream>
#include <iomanip>
using namespace std;

// This function asks the user for input.
void input(int& hour, int& min, char& check);

// This function handles the conversion from 24-hour format
// to 12-hour format. It also returns wether it's AM or PM.
void conversion(int& hour, char& ampm, int min);

// This function simply prints out the results of the conversion.
void output(int hour, int min, char ampm);

int main(){
	char again, ampm, check('Y');
	int hour, min;
	do{
		input(hour, min, check);
		if(check != 'N'){ 					// I don't want to continue if the user entered unsupported numbers.
			conversion(hour, ampm, min);	// I checked if the numbers are supported in the input function.
			output(hour, min, ampm);
		}
		cout << endl << "Do you want to try again?(Y/N) "; // Ask the user if he wants to quit.
		cin >> again;
	}while(again == 'Y' || again == 'y');
	return 0;
}

void input(int& hour, int& min, char& check){
	cout << "What's the hour? ";
	cin >> hour;
	if(hour >= 0 && hour < 24){ 		// If the hour is supported the program asks for minutes.
		cout << "What are the minutes? ";
		cin >> min;
		if (!(min >= 0 && min < 60)){ 	// If the minutes are not supported the program prints an
										// error and sets a "check" variable to N.
			cout << "Those minutes are not supported (0-59)";
			check = 'N';
		}
		else if(check == 'N'){ 			// This statement only runs if the user previously
			check = 'Y';				// entered unsupported minutes.
		}
	}
	else{ 								// This statement runs if the user enters an unsupported hour.
		cout << "That hour is not supported (0-23)";
		check = 'N';
	}
}

void conversion(int& hour, char& ampm, int min){
		if (hour > 12 && hour < 24){ 	// This statement runs if the hour is anywhere
			hour -= 12;					// between (and not including) 12 and 24.
			ampm = 'P';
		}
		else if(hour == 12){			// This is a special case where the hour is 12 at noon.
			ampm = 'P';
		}
		else if(hour == 0){				// This is a special case where the hour is 12 at midnight
			hour = 12;
			ampm = 'A';
		}
		else if(hour > 0 && hour < 12){	// This statement runs if the hour is anywhere between
			ampm = 'A';					// (and not including) 12 at midnight and 12 at noon.
		}
}

void output(int hour, int min, char ampm){
	cout << "The time is: ";
	cout << hour << ":" << setfill('0') << setw(2) << min;
	if(ampm == 'P'){					// These if statements check whether it's
		cout << " PM";					// AM or PM and prints accordingly.
	}
	else if (ampm == 'A'){
		cout << " AM";
	}
}