#include <iostream>
#include <iomanip>
using namespace std;

// This function asks the user for input.
void input(int& hour, int& min);

// This function handles the conversion from 24-hour format
// to 12-hour format. It also returns wether it's AM or PM.
void conversion(int& hour, char& ampm);

// This function simply prints out the results of the conversion.
void output(int hour, int min, char ampm);

int main(){
	char again, ampm;
	int hour, min;
	do{
		input(hour, min);
		conversion(hour, ampm);
		if(ampm != 'N'){ // I don't want to print the results if the user enters hour above 24.
			output(hour, min, ampm);
		}
		cout << endl << "Do you want to try again?(Y/N) "; // Ask the user if he wants to quit.
		cin >> again;
	}while(again == 'Y' || again == 'y');
	return 0;
}

void input(int& hour, int& min){
	cout << "What's the hour? ";
	cin >> hour;
	cout << "What are the minutes? ";
	cin >> min;
}

void conversion(int& hour, char& ampm){
	if (hour > 12 && hour < 24){
		hour -= 12;
		ampm = 'P';
	}
	else if(hour == 12){
		ampm = 'P';
	}
	else if(hour == 24){
		hour -= 12;
		ampm = 'A';
	}
	else if(hour >= 1 && hour < 12){
		ampm = 'A';
	}
	else{
		cout << "That hour is not supported";
		ampm = 'N';
	}
}

void output(int hour, int min, char ampm){
	cout << "The time is: ";
	cout << hour << ":" << setfill('0') << setw(2) << min;
	if(ampm == 'P'){
		cout << " PM";
	}
	else if (ampm == 'A'){
		cout << " AM";
	}
}