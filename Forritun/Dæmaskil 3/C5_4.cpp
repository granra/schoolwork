#include <iostream>
using namespace std;

// This function asks the user for input.
void input(int& base, int& exponent);

// This function takes in a base and exponent and calculates base to the power of the exponent.
double calculation(int base, int exponent);

// This function prints out the outcome.
void output(int base, int exponent, double power);

int main()
{
	int base, exponent;
	double power;
	input(base, exponent);
	power = calculation(base, exponent);
	output(base, exponent, power);
	return 0;
}

void input(int& base, int& exponent){
	cout << "What's the base? ";
	cin >> base;
	cout << "What's the exponent? ";
	cin >> exponent;
}

double calculation(int base, int exponent){
	if(exponent == 0){						// If the exponent is 0 the outcome is always 1.
		return 1;
	}
	else{
		double temp = base;					// A temporary variable I use to calculate the outcome.
		if(exponent > 0){					// If the exponent is more than 0 we duplicate the base.
			for(int i=1; i<exponent; i++){
				temp *= base;
			}
		}
		else{								// If the exponent is less than 0 we divide the base.
			for(int i=1; i>exponent; i--){
				temp /= base;
			}
		}
		return temp;
	}
}

void output(int base, int exponent, double power){
	cout << base << " to the power of " << exponent << " is " << power << endl;
}