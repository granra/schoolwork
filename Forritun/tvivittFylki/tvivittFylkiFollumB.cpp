#include <iostream>
#include <iomanip>
using namespace std;
const int NEMAR = 5;
const int FOG = 3;
void   lesa_einkunnir(int einkunn[][FOG],int nemfj, int fagfj);
void   skrifa_einkunnir(int einkunn[][FOG],int nemfj, int fagfj);

int main()
{

  // tv�v�tt fylki b�i� til.
  int einkunn[NEMAR][FOG];
  // sl�um inn g�gn � fylki�
  lesa_einkunnir(einkunn,NEMAR,FOG);
  skrifa_einkunnir(einkunn,NEMAR,FOG);

  // B� til fylki til a� geyma me�aleinkunnir nemenda
  double medalNem[NEMAR];
  // me�aleinkunnir reikna�ar og settar � einv�tt fylki
  // sem heitir medalNem
  for (int inem=0 ; inem<NEMAR ; inem++){
      int sum=0 ;
      for (int ifag=0 ; ifag<FOG ;ifag++){
        sum += einkunn[inem][ifag];
      }
      medalNem[inem]= static_cast<double>(sum) / FOG;
  }



  // b� til fylki til a� reikna me�aleinkunnir � fagi
  double medalFog[FOG];
  //reikna me�aleinkunnir � fagi og set � fylki� medalFog
  for (int ifag=0 ; ifag<FOG ;ifag++){
    int sum=0 ;
     for (int inem=0 ; inem<NEMAR ; inem++){
        sum += einkunn[inem][ifag];
      }
      medalFog[ifag]= static_cast<double>(sum) / NEMAR;
  }
  // skrifa me�aleinkun � f�gum
  cout << "Medaleinkunn � fogum " << endl;

  for (int ifag=0 ; ifag<FOG ;ifag++){
    cout <<    medalFog[ifag] <<  " " ;
  }
  cout << endl;
  // skrifa me�aleinkunn � hvers nemanda
  cout << "Medaleinkunn nemenda " << endl;

  for (int inem=0 ; inem<NEMAR ;inem++){
    cout <<    medalNem[inem] <<  " " ;
  }
  cout << endl;
  // skrifa allt frekar snyrtilega !!
  cout.precision(2);
  cout << "einkunnir nemenda asamt medaleinkunn nemenda og medaleinkunn i fogum" << endl;
  cout << "Nemandi nr.  ";
  for (int inem=0 ; inem<NEMAR ;inem++){
    cout <<  setw(4) <<  inem+1 ;
  }
  cout << " medaltal " <<endl;
  for (int ifag=0 ; ifag<FOG ;ifag++){
    cout << "fag"<< setw(3) << ifag << "       ";
    for (int inem=0 ; inem<NEMAR ; inem++){
      cout << setw(4) << einkunn[inem][ifag];
    }
    cout << "  " << setw(4) << medalFog[ifag] << endl;
  }
  cout << "Medaltal     " ;
  for (int inem=0 ; inem<NEMAR ;inem++){
    cout << setw(3) << " " << medalNem[inem] ;
  }
  cout << endl;
  return 0;
}
void   lesa_einkunnir(int einkunn[][FOG],int nemfj, int fagfj)
{
  for (int inem=0 ; inem<nemfj ; inem++){
      cout << "Slaid inn einkunnir nemanda nr " << inem+1 << endl;
      for (int ifag=0 ; ifag<fagfj ;ifag++){
        cout << "i fagi nr " << ifag+1 << " " << endl;
        cin >> einkunn[inem][ifag];
      }
  }
}
void   skrifa_einkunnir(int einkunn[][FOG],int nemfj, int fagfj)
{
  // innihald fylkis skrifa� � skj�
  for (int inem=0 ; inem<nemfj ; inem++){
    cout << "Einkunnir nemanda nr " << inem+1 << ": " ;
    for (int ifag=0 ; ifag<fagfj ;ifag++){
      cout << einkunn[inem][ifag] << " ";
    }
    cout << endl;
  }
}

