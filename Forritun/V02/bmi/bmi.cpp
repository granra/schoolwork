#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	int weight;
	double heigth;
	cin >> weight >> heigth;
	cout << fixed << setprecision(2) << weight/(heigth * heigth);
	return 0;
}
