#include <iostream>
using namespace std;

int main()
{
	int n, count(0);
	double *array;
	double avrg, tala, sum(0);

	cin >> n;
	array = new double[n];

	for (int i = 0; i < n; ++i)
	{
		cin >> tala;
		array[i] = tala;
	}

	for (int i = 0; i < n; ++i)
	{
		sum += array[i];
	}

	avrg = sum / n;

	for (int i = 0; i < n; ++i)
	{
		if (array[i] < avrg)
		{
			count++;
		}
	}

	delete[] array;

	cout << count << endl;
	return 0;
}