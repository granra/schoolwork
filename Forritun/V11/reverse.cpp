#include <iostream>
using namespace std;

int main()
{
	int n, tala;
	int *array;
	cin >> n;
	array = new int[n];

	for (int i = 0; i < n; ++i)
	{
		cin >> tala;
		array[i] = tala;
	}

	for (int i = n - 1; i >= 0; --i)
	{
		cout << array[i] << " ";
	}
	cout << endl;

	delete[] array;

	return 0;
}