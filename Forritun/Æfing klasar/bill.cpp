#include <iostream>
#include <string>
using namespace std;
const int MAX_FJOLDI = 10;

class bill
{
private:
	char numer[10];
	int argerd;
	float verd;
public:
	bill();
	void lesa_bilnumer();
	void lesa_argerd();
	void lesa_verd();
	void birta();
	int priceReduction();
};

int main()
{

bill bilar[MAX_FJOLDI];
char svar;
int fjoldi = 0;

do
{
bilar[fjoldi].lesa_bilnumer();
bilar[fjoldi].lesa_argerd();
bilar[fjoldi].lesa_verd();
fjoldi++;
if(fjoldi == MAX_FJOLDI)
{
cout <<" Ekki er hægt að slá inn fleiri bíla " << endl;
break;
}

cout << "Viltu slá inn aftur (j/n) " ;
cin >> svar;
}while(svar !='n');

for(int i=0; i<fjoldi; i++)
bilar[i].birta();

return 0; 
}

bill::bill() {
	numer[0] = ' ';
	argerd = 0;
	verd = 0;
}

void bill::lesa_bilnumer() {
	cin >> numer;
}

void bill::lesa_argerd() {
	cin >> argerd;
}

void bill::lesa_verd() {
	cin >> verd;
}

int bill::priceReduction() {
	int age = 2013 - argerd;
	int reduction;
	if(age <= 3) {
		reduction = 15;
	}
	else if(age <= 6) {
		reduction = 10;
	}
	else if(age <= 7 && age >= 11) {
		reduction = 5;
	}
	else {
		reduction = 0;
	}

	return reduction;
}

void bill::birta() {
	cout << "Bílnúmer: " << numer << endl;
	cout << "Árgerð: " << argerd << endl;
	cout << "Verð: " << verd << endl;
	cout << "Verðis rýrnun: " << priceReduction() << '%' << endl;
}