#include <iostream>
using namespace std;

class timi
{
private:
	int hours;
	int minutes;
	int seconds;
public:
	timi(int hour, int minute, int second);
	friend ostream& operator << (ostream& out, const timi& time);
	friend timi operator = (timi& time);
};

void main(){
	timi t1(16,20,0);
	// New instance of the class
	cout << t1 << endl;
	// Here the time 16:20:00, is displayed
	t1=timi(0,0,14655);
	cout << t1 << endl;
	t1+=timi(0,120,0);
	// += operator is used
	cout << t1 << endl;
	t1 = t1-timi(0,0,15);
	// - operator used
	cout << t1 << endl;
	t1 = timi(6,0,0) – timi(0,0,1);
	cout << t1 << endl;
	t1 = timi(0) – timi(0,0,1);
	cout << t1 << endl;
	t1 +=1;
	// add one hour
	cout << t1 << endl;
} 

timi::timi(int hour, int minute, int second) {
	hours = hour;
	minute = minutes;
	second = seconds;
}

ostream& operator << (ostream& out, const clock& time) {
	out << hour << ":" << minute << ":" << second << endl;

	return out;
}

friend timi operator = (timi& time) {
	
}