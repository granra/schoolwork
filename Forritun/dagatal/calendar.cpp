#include <iostream>
#include <cmath>
using namespace std;

bool erHlaupaAr(int artal){
	bool hlar=false;
	if(artal%4==0){
		hlar=true;
	}
	return hlar;
}

int fjoldiDagaMan(int man, int ar){
	if(man==4 || man==6 || man==9 || man==11){
		return 30;
	}
	else if(man==2 && erHlaupaAr(ar)){
		return 29;
	}
	else if(man==2){
		return 28;
	}
	else{
		return 31;
	}
}

int main()
{
	cout << fjoldiDagaMan(4,2000);
	return 0;
}