#include <iostream>
using namespace std;

class IntList {
private:
    int numbers[20];
    int size;
public:
    IntList();
    void add(int number);
    friend ostream& operator << (ostream& out, const IntList& list);
    friend bool operator > (const IntList& left, const IntList& right);
    friend IntList operator + (const IntList& left, const IntList& right);
    int getSize();
    bool full();
};

int main()
{
    IntList list1, list2, list3, list4;   // Declare 4 integer lists

    for (int i = 0; i < 10; i++)          // Populate the first list
    {
        list1.add(i);
    }

    for (int i = 0; i < 5; i++)           // Populate the second list
    {
        list2.add(i * 2);
    }

    cout << list1;                        // Output the first list to the screen
    cout << list2;                        // Output the second list to the screen

    if (list1 > list2)
    {
        cout << "List 1 is larger than list 2" << endl;
    }
    else
    {
        cout << "List 1 is not larger than list 2" << endl;
    }

    list3 = list1 + list2;                // Add the first two lists
    cout << list3;                        // And output the result to the screen

    cout << "The size of list 3 is: " << list3.getSize() << endl;

    for (int i = 10; i < 25; i++)
    {
        list1.add(i);                     // Put more entries into list 1
    }

    cout << list1;                        // And output it

    if (list1.full())
    {
        cout << "List 1 is full!" << endl;
    }

    list4 = list3;                        // Assign the third list to the fourth
    cout << list4;

    return 0;
}

IntList::IntList() {
    size = 0;
}

void IntList::add(int number) {
    if (!(size > 19))
    {
        numbers[size] = number;
        size++;
    }
}

ostream& operator << (ostream& out, const IntList& list) {
    for (int i = 0; i < list.size; ++i)
    {
        out << list.numbers[i] << " ";
    }
    out << endl;

    return out;
}

bool operator > (const IntList& left, const IntList& right) {
    if (left.size > right.size)
    {
        return 1;
    }

    return 0;
}

IntList operator + (const IntList& left, const IntList& right) {
    int smaller;
    if (left > right)
    {
        smaller = right.size;
    }
    else {
        smaller = left.size;
    }
    IntList temp;
    for (int i = 0; i < smaller; ++i)
    {
        temp.numbers[i] = left.numbers[i] + right.numbers[i];
    }
    temp.size = smaller;

    return temp;
}

int IntList::getSize() {
    return size;
}

bool IntList::full() {
    if (size > 19)
    {
        return 1;
    }
    return 0;
}