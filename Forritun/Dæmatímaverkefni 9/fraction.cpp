#include <iostream>
using namespace std;

class Fraction {
private:
    int numerator, denominator;
public:
    void input_numerator();
    void input_denominator();
    friend bool operator < (const Fraction& left, const Fraction& right);
    friend ostream& operator << (ostream& out, const Fraction& right);
};

int main()
{
    const int n = 10;

    Fraction arr[n];

    // Input ten fractions into the array
    for (int i = 0; i < n; i++)
    {
        arr[i].input_numerator();
        arr[i].input_denominator();
    }

    // Bubble sort the fractions; do not try this at home
    for (int i = 0; i < n; i++)
    {
        for (int j = 1; j < n; j++)
        {
            if (arr[j] < arr[j - 1])
            {
                Fraction tmp = arr[j];
                arr[j] = arr[j - 1];
                arr[j - 1] = tmp;
            }
        }
    }

    // Output the fractions
    for (int i = 0; i < n; i++)
    {
        cout << arr[i];
    }

    return 0;
}

void Fraction::input_numerator() {
    cin >> numerator;
}

void Fraction::input_denominator() {
    cin >> denominator;
}

bool operator < (const Fraction& left, const Fraction& right) {
    if ((static_cast<double>(left.numerator) / left.denominator) 
        < (static_cast<double>(right.numerator) / right.denominator))
    {
        return 1;
    }

    return 0;
}

ostream& operator << (ostream& out, const Fraction& right) {
    out << right.numerator << "/" << right.denominator << endl;

    return out;
}