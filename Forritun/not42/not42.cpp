#include <iostream>
using namespace std;

int main()
{
	int n;
	cin >> n;
	if(n == 42){
		cout << "The Answer to the Ultimate Question of Life, the Universe, and Everything is " << n << ".";
	}
	else {
		cout << "The Answer to the Ultimate Question of Life, the Universe, and Everything is not " << n << ".";
	}
	return 0;
}