#include <iostream>
using namespace std;

double calcAvg(int a[], int n);

int main()
{
	int n, *a, count = 0;
	double avg;
	cin >> n;
	a = new int[n];

	for (int i = 0; i < n; ++i)
	{
		cin >> a[i];
	}

	avg = calcAvg(a, n);

	for (int i = 0; i < n; ++i)
	{
		if (a[i] < avg)
		{
			count++;
		}
	}

	cout << count;

	return 0;
}

double calcAvg(int a[], int n) {
	int sum = 0;

	for (int i = 0; i < n; ++i)
	{
		sum += a[i];
	}

	return static_cast<double>(sum) / n;
}