#include <iostream>
using namespace std;

class timi
{
private:
	int hour;
	int minute;
	int second;
public:
	timi(int hr, int min, int sec);
	timi(int hr);
	friend ostream& operator << (ostream& out, const timi& t);
	timi operator = (const timi& t);
	friend void operator += (timi& left, const timi& right);
	friend void operator += (timi& left, int amount);
	friend timi operator - (timi& left, const timi& right);
};

int main(){
	timi t1(16,20,0);
// New instance of the class
	cout << t1 << endl;
// Here the time 16:20:00, is displayed
	t1=timi(0,0,14655);
	cout << t1 << endl;
	t1+=timi(0,120,0);
// += operator is used
	cout << t1 << endl;
	t1 = t1-timi(0,0,15);
// - operator used
	cout << t1 << endl;
	t1 = timi(6,0,0); // – timi(0,0,1);
	cout << t1 << endl;
	t1 = timi(0); // – timi(0,0,1);
	cout << t1 << endl;
	t1 +=1;
// add one hour
	cout << t1 << endl;

	return 0;
} 

timi::timi(int hr, int min, int sec) {
	hour = hr;
	minute = min;
	second = sec;
}

timi::timi(int hr) {
	hour = hr;
	minute = 0;
	second = 0;
}

ostream& operator << (ostream& out, const timi& t) {
	out << t.hour << ":" << t.minute << ":" << t.second << endl;
	return out;
}

timi timi::operator = (const timi& t) {
        timi temp(0);
        temp.hour = t.hour;
        temp.minute = t.minute;
        temp.second = t.second;
 
        return temp;
}

void operator += (timi& left, const timi& right) {
	left.hour += right.hour;
	left.minute += right.minute;
	left.second += right.second;
}

void operator += (timi& left, int amount) {
	left.hour += amount;
}

timi operator - (timi& left, const timi& right) {
	left.hour - right.hour;
	left.minute - right.minute;
	left.second - right.second;

	return left;
}