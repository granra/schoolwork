#include <iostream>
using namespace std;

// TODO: function declaratin comes here
bool increasing(int* arr, int n);

int main()
{
    int arr[100], n;
    while (cin >> n)
    {
        for (int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        cout << (increasing(arr, n) ? "increasing" : "not increasing") << endl;
    }

    return 0;
}

// TODO: function implementation comes here
bool increasing(int* arr, int n) {
    for (int i = 1; i < n; ++i)
    {
        if (arr[i - 1] > arr[i])
        {
            return 0;
        }
    }
    return 1;
}