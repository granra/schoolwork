#include <iostream>
using namespace std;

// TODO: implement the is_leap_year function here

bool is_leap_year(int year) {
    if (year % 4 == 0)
    {
        if (year % 100 == 0 && year % 400 != 0)
        {
            return 0;
        }
        return 1;
    }
    return 0;
}

// DO NOT EDIT BELOW THIS LINE
// ---------- snip -----------

int main()
{
    int year;
    while (cin >> year)
    {
        if (is_leap_year(year))
        {
            cout << year << " is a leap year" << endl;
        }
        else
        {
            cout << year << " is not a leap year" << endl;
        }
    }

    return 0;
}