#include <iostream>
#include <vector>
using namespace std;

// TODO: function declaratin comes here
bool increasing(vector<int> &arr);

int main()
{
    int n;
    while (cin >> n)
    {
        vector<int> arr(n);
        for (int i = 0; i < n; i++)
        {
            cin >> arr[i];
        }

        cout << (increasing(arr) ? "increasing" : "not increasing") << endl;
    }

    return 0;
}

// TODO: function implementation comes here

bool increasing(vector<int> &arr) {
    int size = arr.size();
    for (int i = 1; i < size; ++i)
    {
        if (arr[i-1] > arr[i])
        {
            return false;
        }
    }

    return true;
}