#include <iostream>
#include <vector>
using namespace std;

int main()
{
	vector<int> range;
	int a, b;

	cin >> a >> b;

	for (int i = a; i <= b; ++i)
	{
		bool prime = 1;
		for (int j = 2; j < i; ++j)
		{
			if (i % j == 0)
			{
				prime = 0;
			}
		}

		if (i != 1)
		{
			if (prime)
			{
				range.push_back (i);
			}
		}
		
	}

	for (int i = 0; i < range.size(); ++i)
	{
		cout << range[i] << endl;
	}

	return 0;
}