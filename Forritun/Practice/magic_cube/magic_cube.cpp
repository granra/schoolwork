#include <iostream>
using namespace std;

void calcSums(int** arr, int* sums, int n);

bool magic(int* arr, int n);

int main()
{
	int n;
	cin >> n;
	int** arr = new int*[n];

	// Input
	for (int i = 0; i < n; ++i)
	{
		arr[i] = new int[n];

		for (int k = 0; k < n; ++k)
		{
			cin >> arr[i][k];
		}
	}
	
	int* sums = new int[2*n + 2];

	// Calculate sums
	calcSums(arr, sums, n);

	// Magic square check
	if(magic(sums, n)) {
		cout << "galdraferningur";
	}
	else {
		cout << "ekki galdraferningur";
	}

	// Delete
	for (int i = 0; i < n; ++i)
	{
		delete[] arr[i];
	}
	delete[] arr;

	return 0;
}

void calcSums(int** arr, int* sums, int n) {
	int i, l = 0;

	for (int i = 0; i < (2*n + 2); ++i)
	{
		sums[i] = 0;
	}

	for (i = 0; i < n; ++i)
	{
		for (int k = 0; k < n; ++k)
		{
			sums[i] += arr[i][k];
		}
	}

	for (int j = 0; j < n; ++j)
	{
		sums[i] += arr[j][j];
	}
	i++;

	for (int j = 0; j < n; ++j)
	{
		for (int l = 0; l < n; ++l)
		{
			sums[i] += arr[l][j];
		}
		i++;
	}

	for (int j = n -1; j >= 0; --j)
	{
		sums[i] += arr[l][j];
		l++;
	}
}

bool magic(int* arr, int n) {
	for (int i = 1; i < (2*n + 2); ++i)
	{
		if (arr[i - 1] != arr[i])
		{
			return 0;
		}
	}
	return 1;
}