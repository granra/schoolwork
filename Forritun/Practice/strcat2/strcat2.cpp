#include <iostream>
using namespace std;

// TODO: function declaratin comes here
char strcat2(char a[], char b[]);
int strlen(char a[]);

int main()
{
    char *a = new char[2010], *b = new char[1000];
    while (cin.getline(a, 1000) && cin.getline(b, 1000))
    {
        cout << "a = \"" << a << "\";" << endl;
        cout << "strcat2(a, \"" << b << "\");" << endl;
        strcat2(a, b);
        cout << "a = \"" << a << "\";" << endl << endl;
    }

    delete[] a;
    delete[] b;
    return 0;
}

char strcat2(char a[], char b[]) {
	int length = strlen(a);
	int i = 0, k = length;
	while (b[i] != 0) {
		a[k] = b[i];
		i++;
		k++;
	}
	a[k] = 0;
}

int strlen(char a[]) {
	int count = 0;

	while (a[count] != 0) {
		count++;
	}

	return count;
}