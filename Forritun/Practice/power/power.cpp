#include <iostream>
using namespace std;

void input(int& a, int& b) {
	cin >> a >> b;
}

double calcPwr(int base, int exponent) {
	if (exponent == 0) {
		return 1;
	}
	double tmp = base;
	for (int i = 0; i < exponent - 1; ++i)
	{
		tmp *= base;
	}
	for (int i = exponent; i <= 0; ++i)
	{
		tmp /= base;
	}

	return tmp;
}

int main()
{
	int base, exponent;
	double pwr;

	input(base, exponent);

	pwr = calcPwr(base, exponent);

	cout << pwr;

	return 0;
}