#include <iostream>
#include <cmath>
using namespace std;

double calcAvg(double arr[], int n);

double calcSum(double numbers[], int n);

int main()
{
	double *numbers;
	int n;
	cin >> n;
	numbers = new double[n];

	for (int i = 0; i < n; ++i)
	{
		cin >> numbers[i];
	}

	cout << sqrt(calcSum(numbers, n));

	delete[] numbers;

	return 0;
}

double calcAvg(double arr[], int n) {
	double sum = 0;

	for (int i = 0; i < n; ++i)
	{
		sum += arr[i];
	}

	return sum / n;
}

double calcSum(double numbers[], int n) {
	double sum = 0, avg = calcAvg(numbers, n);

	for (int i = 0; i < n; ++i)
	{
		double ins = numbers[i] - avg;
		sum += ins * ins;
	}

	return sum / n;
}