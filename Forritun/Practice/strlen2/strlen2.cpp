#include <iostream>
using namespace std;

// TODO: function declaratin comes here
int strlen2(char arr[]);

int main()
{
    char s[1000];
    while (cin.getline(s, 1000))
    {
        cout << "strlen2(\"" << s << "\") = " << strlen2(s) << endl;
    }

    return 0;
}

// TODO: function implementation comes here
int strlen2(char arr[]) {
	int count = 0;

	while(arr[count] != 0) {
		count++;
	}
	return count;
}