#include <iostream>
using namespace std;

int main()
{
	int a, b, erPrime;
	cin >> a >> b;
	for(int i=a; i<=b; i++){
		erPrime = 1;
		for(int j=2; j<=i; j++){
			if(i % j == 0 && j != i){
				erPrime = 0;
			}
		}
		if(erPrime == 1 && i != 1){
			cout << i << endl;
		}
	}
	return 0;
}