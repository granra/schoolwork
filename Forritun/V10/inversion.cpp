#include <iostream>
using namespace std;

int count_inversion(int numbers[], int n);

int main()
{
	int n, numbers[100];
	cin >> n;
	for (int i = 0; i < n; ++i)
	{
		cin >> numbers[i];
	}

	cout << count_inversion(numbers, n);
	return 0;
}

int count_inversion(int numbers[], int n){
	int count(0), highest(0);
	for (int i = 0; i < n; ++i)
	{
		if(numbers[i] > highest){
			highest = numbers[i];
		}
		else {
			count++;
		}
	}
	return count + 1;
}