#include <iostream>
using namespace std;

// TODO: function declaratin comes here
int strlen2(char s[]);

int main()
{
    char s[1000];
    while (cin.getline(s, 1000))
    {
        cout << "strlen2(\"" << s << "\") = " << strlen2(s) << endl;
    }

    return 0;
}

// TODO: function implementation comes here
int strlen2(char s[]){
	int count(0), i(0);
	while(s[i] != 0){
		count++;

		i++;
	}
	return count;
}